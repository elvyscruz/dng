const exec = require('child_process').exec;

const fs = require('fs');

const letters = 3;
const prefix = '';
const postfix = 'gpt';
const attempts = 15;

function rletter() {
  return String.fromCharCode(Math.round(Math.random() * (122 - 97) + 97));
}

function makeWord(n) {
  let word = '';

  for (let i = 0; i < n; i++) {
    word = word + rletter();
  }

  return word;
}

function dnac() {
  const name = makeWord(letters);
  console.log(`verifying ${prefix}${name}${postfix}.com`);
  exec(
    `/usr/bin/whois -H ${prefix}${name}${postfix}.com; sleep 1`,
    function (err, stdout, stderr) {
      if (err) {
        console.log('hubo un error en elpago.comando ' + err);
        return;
      }
      if (/No match/.test(stdout)) {
        console.log(`${prefix}${name}${postfix}.com is available`);
        fs.appendFile(
          'found.txt',
          `${prefix}${name}${postfix}.com \n`,
          function (err) {
            if (err) {
              console.log(err);
            }
            // console.log('Domain name saved')
          }
        );
      }
    }
  );
}

for (let i = 0; i < attempts; i++) {
  dnac();
}
